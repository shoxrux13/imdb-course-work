package com.imdb.reveiw_rating;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(basePackages = "uz.pdp.clients")
public class CodeExecutorApplication {
    public static void main(String[] args) {
        SpringApplication.run(CodeExecutorApplication.class,args);
    }
}

