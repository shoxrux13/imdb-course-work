package com.imdb.movie_service.controller;

import org.springframework.http.ResponseEntity;
import uz.pdp.clients.data_transfer.dto.MovieDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.clients.data_transfer.DataTransferClient;

@RestController
@RequestMapping("/api/movie")
public class MovieController {


    @Autowired
    DataTransferClient transferClient;


    @PostMapping
    public HttpEntity<?> saveMovie(@RequestPart MovieDto movieDto , @RequestPart MultipartFile trailer){
        movieDto.setTrailer(trailer);
        return ResponseEntity.ok(transferClient.saveMovie(movieDto));
    }

    @PutMapping
    public HttpEntity<?> editMovie(@RequestBody MovieDto movieDto){
        return ResponseEntity.ok(transferClient.editMovie(movieDto));
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteMovie(@PathVariable Long id){
        return ResponseEntity.ok(transferClient.deleteMovie(id));
    }

    @GetMapping
    public HttpEntity<?> getAll(
            @RequestParam(required = false, defaultValue = "20") int size,
            @RequestParam(required = false, defaultValue = "1") int page
    ){
        return ResponseEntity.ok(transferClient.getAll(size,page));
    }
}
