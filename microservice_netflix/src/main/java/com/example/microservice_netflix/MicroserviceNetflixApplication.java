package com.example.microservice_netflix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class MicroserviceNetflixApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicroserviceNetflixApplication.class, args);
    }

}
