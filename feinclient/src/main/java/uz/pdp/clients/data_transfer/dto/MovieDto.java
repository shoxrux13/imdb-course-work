package uz.pdp.clients.data_transfer.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Date;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MovieDto {

    Long id;

    String title;

    String description;

    Integer duration ;

    Date releaseDate;

    Double budget;

    List<Long> genres;

    List<Long> actors;

    List<Long> directors;

    MultipartFile trailer;

}
