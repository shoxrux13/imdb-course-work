package uz.pdp.clients.data_transfer;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import uz.pdp.clients.data_transfer.dto.MovieDto;

import java.util.List;


@FeignClient("dataTransfer")
public interface DataTransferClient {

    @PostMapping("/api/movie-data")
    String saveMovie(MovieDto movieDto);

    @PutMapping("/api/movie-data")
    String editMovie(@RequestBody MovieDto movieDto);


    @DeleteMapping("/api/movie-data/{id}")
    String deleteMovie(@PathVariable Long id);

    @GetMapping("/api/movie-data")
    List<?> getAll(
            @RequestParam(required = false, defaultValue = "20") int size,
            @RequestParam(required = false, defaultValue = "1") int page
    );
}
