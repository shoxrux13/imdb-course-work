package com.imdb.data_transfer.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.imdb.data_transfer.model.Attachment;
import com.imdb.data_transfer.model.AttachmentContent;
import com.imdb.data_transfer.model.Movie;
import com.imdb.data_transfer.repository.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.clients.data_transfer.dto.MovieDto;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@AllArgsConstructor
@Service
public class DataService {
    ObjectMapper objectMapper;
    MovieRepository movieRepository;
    ActorRepository actorRepository;
    GenreRepository genreRepository;
    DirectorRepository directorRepository;
    AttachmentRepository attachmentRepository;
    AttachmentContentRepository attachmentContentRepository;


    public String saveMovie(MovieDto movieDto) {
        try {
            Movie movie = Movie.builder()
                    .title(movieDto.getTitle())
                    .description(movieDto.getDescription())
                    .budget(movieDto.getBudget()).duration(movieDto.getDuration()).releaseDate(movieDto.getReleaseDate().toLocalDate())
                    .actors(actorRepository.findAllById(movieDto.getActors()))
                    .genres(genreRepository.findAllById(movieDto.getGenres()))
                    .directors(directorRepository.findAllById(movieDto.getDirectors()))
                    .build();

            Attachment attachment = new Attachment();
            attachment.setSize(movieDto.getTrailer().getSize());
            attachment.setRealName(movieDto.getTrailer().getOriginalFilename());
            attachmentRepository.save(attachment);
            AttachmentContent attachmentContent = new AttachmentContent();
            attachmentContent.setBytes(movieDto.getTrailer().getBytes());
            attachmentContent.setAttachment(attachment);
            attachmentContentRepository.save(attachmentContent);
            movie.setTrailer(attachment);
            return "movie "+ movie.getTitle()+" successfully saved";
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    public String editMovie(MovieDto movieDto) {
        Optional<Movie> byId = movieRepository.findById(movieDto.getId());
        if (byId.isPresent()) {
            Movie movie = byId.get();
            movie = Movie.builder()
                    .title(movieDto.getTitle())
                    .description(movieDto.getDescription())
                    .budget(movieDto.getBudget()).duration(movieDto.getDuration()).releaseDate(movieDto.getReleaseDate().toLocalDate())
                    .actors(actorRepository.findAllById(movieDto.getActors()))
                    .genres(genreRepository.findAllById(movieDto.getGenres()))
                    .directors(directorRepository.findAllById(movieDto.getDirectors()))
                    .build();
            movie.setId(movie.getId());
            movieRepository.save(movie);
            return "updated";
        }else {
            return "movie id not found";
        }
    }

    public String delete(Long id) {
        movieRepository.deleteById(id);
        return "delete movie";
    }

    public List<?> getAll(int size, int page) {
        try {
            List<Map<String, Object>> allMovie = movieRepository.getAllMovie(size, page);
            return allMovie;
        }catch (Exception e){
            return new ArrayList<>();
        }

    }





    public List<Map<String, Object>> StringToJsonArray(List<Map<String, Object>> data, String... a){
        List<Map<String, Object>> res = new ArrayList<>();
        for (Map<String, Object> datum : data) {
            res.add(stringToJson(datum, a));
        }
        return res;
    }
    public Map<String, Object> stringToJson(Map<String , Object> map, String... a){
        Map<String, Object> res = new HashMap<>();
        for (String s : a) {
            try {
                List list = objectMapper.readValue(map.get(s).toString(), List.class);
                res.put(s, list);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            map.keySet().stream().filter(s2 -> !Arrays.stream(a).toList().contains(s2)).forEach(s1 -> res.put(s1, map.get(s1)));
        }
        return res;
    }


}











