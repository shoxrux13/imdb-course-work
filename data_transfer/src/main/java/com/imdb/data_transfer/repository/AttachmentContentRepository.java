package com.imdb.data_transfer.repository;

import com.imdb.data_transfer.model.AttachmentContent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent, Long> {


}
