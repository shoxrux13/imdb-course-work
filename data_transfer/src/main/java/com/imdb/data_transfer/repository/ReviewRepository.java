package com.imdb.data_transfer.repository;

import com.imdb.data_transfer.model.Director;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReviewRepository extends JpaRepository<Director, Long> {




}
