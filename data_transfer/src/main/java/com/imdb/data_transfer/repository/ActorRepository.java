package com.imdb.data_transfer.repository;

import com.imdb.data_transfer.model.Actor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActorRepository extends JpaRepository<Actor, Long> {




}
