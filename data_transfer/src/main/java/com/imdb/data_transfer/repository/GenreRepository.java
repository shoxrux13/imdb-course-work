package com.imdb.data_transfer.repository;

import com.imdb.data_transfer.model.Genre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenreRepository extends JpaRepository<Genre, Long> {




}
