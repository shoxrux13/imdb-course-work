package com.imdb.data_transfer.repository;

import com.imdb.data_transfer.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

public interface MovieRepository extends JpaRepository<Movie, Long> {

    @Query(nativeQuery = true, value = """
            select m.id, budget,
             description,
             duration,
             release_date as "releaseDate",
             title,
             cast(json_agg(jsonb_build_object('fullName', a.full_name, 'id', a.id)) as text) as actors,
             cast(json_agg(jsonb_build_object('fullName', d.full_name, 'id', d.id)) as text) as directors,
             cast(json_agg(jsonb_build_object('name', g.name, 'id', g.id)) as text)as geners
              from
             movies m
             join movies_actors ma on m.id = ma.movie_id
             join actors a on ma.actor_id = a.id
             join movies_genres mg on m.id = mg.movie_id
             join genres g on mg.genre_id = g.id
             join movies_directors md on m.id = md.movie_id
             join directors d on md.director_id = d.id
             group by m.id limit :size offset :size*:page
            """)
    List<Map<String, Object>> getAllMovie(int size, int page);
}
