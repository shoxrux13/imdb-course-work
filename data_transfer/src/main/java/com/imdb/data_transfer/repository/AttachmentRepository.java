package com.imdb.data_transfer.repository;

import com.imdb.data_transfer.model.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttachmentRepository extends JpaRepository<Attachment, Long> {


}
