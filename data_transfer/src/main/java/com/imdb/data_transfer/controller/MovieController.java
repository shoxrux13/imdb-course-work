package com.imdb.data_transfer.controller;

import com.imdb.data_transfer.service.DataService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.clients.data_transfer.dto.MovieDto;

import java.util.List;

@RestController
@RequestMapping("/api/movie-data")
@AllArgsConstructor
public class MovieController {


    DataService dataService;

    @PostMapping
    public String  saveMovie(@RequestBody MovieDto movieDto){
        return dataService.saveMovie(movieDto);
    }

    @PutMapping
    public String editMovie(@RequestBody MovieDto movieDto){
        return dataService.editMovie(movieDto);
    }

    @DeleteMapping("/api/movie-data/{id}")
    String deleteMovie(@PathVariable Long id){
        return dataService.delete(id);
    }

    @GetMapping
    public List<?> getAll(
            @RequestParam(required = false, defaultValue = "20") int size,
            @RequestParam(required = false, defaultValue = "1") int page

    ){
        return dataService.getAll(size, page-1);
    }


}
