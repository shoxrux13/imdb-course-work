package com.imdb.data_transfer.model;
import com.imdb.data_transfer.model.base.AbsEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity(name = "actors")
public class Actor extends AbsEntity {

    @Column(name = "full_name")
    String full_name;

    @Column(columnDefinition = "text")
    String bio;

    LocalDateTime date_of_birth;

    String birth_place;

    @ManyToMany
    @JoinTable(name = "movies_actors",
            joinColumns = @JoinColumn(name = "actor_id"),
            inverseJoinColumns = @JoinColumn(name = "movie_id"))
    List<Movie> movies = new ArrayList<>();

    @OneToMany
    List<Attachment> attachment;

}
