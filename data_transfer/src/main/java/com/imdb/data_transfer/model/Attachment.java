package com.imdb.data_transfer.model;
import com.imdb.data_transfer.model.base.AbsEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity(name = "attachments")
public class Attachment extends AbsEntity {
    Long size;

    String realName;
}
