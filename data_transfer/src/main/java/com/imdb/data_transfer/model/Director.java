package com.imdb.data_transfer.model;
import com.imdb.data_transfer.model.base.AbsEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity(name = "directors")
public class Director extends AbsEntity {

    String fullName;

    @Column(columnDefinition = "text")
    String bio;

    String birthPlace;

    @ManyToMany
    @JoinTable(name = "movies_directors",
            joinColumns = @JoinColumn(name = "director_id"),
            inverseJoinColumns = @JoinColumn(name = "movie_id"))
    List<Movie> movies = new ArrayList<>();
}
