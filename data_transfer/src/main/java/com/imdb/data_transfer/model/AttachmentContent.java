package com.imdb.data_transfer.model;
import com.imdb.data_transfer.model.base.AbsEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@AllArgsConstructor
@NoArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity(name = "attachment_contents")
public class AttachmentContent extends AbsEntity {
    @Column(columnDefinition = "bytea")
   byte[] bytes;

    @OneToOne
    Attachment attachment;
}
