package com.imdb.data_transfer.model;

import com.imdb.data_transfer.model.base.AbsEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@AllArgsConstructor
@NoArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity(name = "ratings")
public class Rating extends AbsEntity {


    int value;

    @ManyToOne
    User user;

    @ManyToOne
    Movie movie;

}
