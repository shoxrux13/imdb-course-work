package com.imdb.data_transfer.model.enums;

public enum Role {
    ADMIN,
    USER
}
